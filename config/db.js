const mongoose = require('mongoose');

mongoose.connect('mongodb://mash:mash007@ds119503.mlab.com:19503/aibluck', { useNewUrlParser: true }, (err) => {
	if (err) {
		console.log('Problem with DB connection');
	}

	console.log('Connection Successful');
});

mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

module.exports = mongoose;
