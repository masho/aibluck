const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
	username: String,
	phone_number: {
		type: String,
		unique: true
	},
	first_name: String
});

module.exports = mongoose.model('User', userSchema);
