const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const transactionSchema = new Schema({
	trans_id: String,
	trans_type: String,
	username: String,
	crypto_amount: String,
	naira_amount: String,
	dollar_amount: String,
	crypto_type: String,
	complete: {
		type: Boolean,
		default: false
	},
	comment: String,
	wallet_address: String,
	exchange_rate: String,
	coin_value: String,
	Start_time: {
		type: Date,
		default: Date.now
	},
	End_time: Date
});

module.exports = mongoose.model('Transaction', transactionSchema);
