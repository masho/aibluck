const express = require('express');
const router = express.Router();
const crypto = require('crypto');
const Pusher = require('pusher');

const pusher = new Pusher({
	appId: '626797',
	key: '7e4f286ee87956961aa1',
	secret: '49a35f2ad0fd13729f1b',
	cluster: 'eu',
	encrypted: true
});

const Transaction = require('../models/Transaction');

const HASH_KEY = 'sk_test_87e1f12f16cb33227e849c9eccd735a2e259e92a';

router.post('/', async (req, res) => {
	let hash = crypto.createHmac('sha512', HASH_KEY).update(JSON.stringify(req.body)).digest('hex');
	if (hash !== req.headers['x-paystack-signature']) {
		res.sendStatus(403);
	}
	let response = req.body;
	let success = response.event === 'charge.success' ? true : false; //input in complete slot
	let id = response.data.id; //input in comment slot
	let trans_id = response.data.reference;

	try {
		let transaction = await Transaction.findOneAndUpdate(
			{ trans_id },
			{ complete: success, comment: String(id), End_time: Date.now() }
		);
		if (transaction) {
			console.log('Transaction Complete');
			pusher.trigger('aibluck-pusher', trans_id, {
				message: 'success'
			});
		} else {
			console.log('Problem with Updating Transaction');
		}
		//console.log(event);
		res.sendStatus(200);
	} catch (error) {
		console.log(error);
		console.log('There is an issue with the updating');
	}
});

router.get('/', (req, res) => {
	let response = req.query.resp;
	if (!response) res.status(404).send('Unauthorized');
	let resp = JSON.parse(response);
	let status = resp.data.respcode;
	if (status === '00') {
		//console.log(re);
		res.status(200);
	}
});

module.exports = router;
