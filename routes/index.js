const express = require('express');
const router = express.Router();

const Transaction = require('../models/Transaction');
const User = require('../models/User');

/* GET home page. */
router.get('/', async (req, res) => {
	let trans_id = req.query.id;

	let transaction = await Transaction.findOne({ trans_id });
	if (transaction) {
		//console.log(transaction);
		res.status(200).render('index', { title: `Transaction ${trans_id}`, trans: transaction });
	} else {
		res.status(200).render('index', { title: 'No Record!', id: trans_id });
	}
});

module.exports = router;
